package main;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.util.converter.IntegerStringConverter;
import java.net.URL;
import java.util.ResourceBundle;

public class BoxesList implements Initializable {

    private ObservableList<Box> boxes = FXCollections.observableArrayList();

    @FXML
    TableView table;

    @FXML
    TableColumn<Box,Integer> size;

    @FXML
    Button add;

    @FXML
    Button remove;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        table.setItems(boxes);
        boxes.add(new Box(10));
        boxes.add(new Box(14));
        boxes.add(new Box(31));
        boxes.add(new Box(42));
        boxes.add(new Box(29));

        size.setCellFactory(TextFieldTableCell.forTableColumn((new IntegerStringConverter())));
        size.setOnEditCommit(event->event.getRowValue().setSize(event.getNewValue()));
    }

    @FXML
    private void Add(ActionEvent event){
        boxes.add(new Box());
    }

    @FXML
    private void Remove(ActionEvent event){
        if (table.getSelectionModel().getSelectedIndex() >= 0) {
            boxes.remove(table.getSelectionModel().getSelectedIndex());
            table.getSelectionModel().clearSelection();
        }
    }
}
