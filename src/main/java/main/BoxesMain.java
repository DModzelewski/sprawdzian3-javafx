package main;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Group;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

import java.net.URL;
import java.util.ResourceBundle;

public class BoxesMain extends Application {
    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage stage) throws Exception {

        URL fxml = this.getClass().getResource("/boxes_list.fxml");

        Parent root = FXMLLoader.load(fxml, null);
        Scene scene = new Scene(root, 600, 600, Color.WHITE);
        stage.setScene(scene);
        stage.show();
    }
}
